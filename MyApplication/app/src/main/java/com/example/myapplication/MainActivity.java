package com.example.myapplication;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.ImageButton;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        configureMessageButton();
        configurePerscriptionButton();

    }

    private void configureMessageButton() {
        ImageButton messagebutton = (ImageButton) findViewById(R.id.imageButton);
        messagebutton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(MainActivity.this, MessageActivity.class));

            }
        });

    }

    private void configurePerscriptionButton() {
        ImageButton perscriptionbutton = (ImageButton) findViewById(R.id.buttonPerscription);
        perscriptionbutton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(MainActivity.this, PerscriptionActivity.class));
            }
        });
    }
}